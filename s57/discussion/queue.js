let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method

    collection[collection.length] = element;
    return collection;
}

function dequeue() {
    // In here you are going to remove the last element in the array

   let newCollection = [];

   newCollection[newCollection.length] = collection[collection.length -1]

    collection = newCollection;
    return collection;
}

function front() {
    // In here, you are going to remove the first element

    const get = collection[0]; 

    return get;

}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements 
    
    return collection.length;
}

function isEmpty() {
    //it will check whether the function is empty or not
    if(collection.length === 0) {
        return true
    }else
        return false

}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};