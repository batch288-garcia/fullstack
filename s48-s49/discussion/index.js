// console.log('Heloo')


// Fecth keyword
	// fetch('url', {options})

	
	// Get post data
	fetch('https://jsonplaceholder.typicode.com/posts', {
		
	})
	.then(result => result.json())
	.then(response => {

		// the response is an array of object

		console.log(response);
		showPosts(response);	
	})

	

	const showPosts = (posts) => {
		console.log(typeof posts);

		let entries = '';

		posts.forEach((post) => {
			entries += `
				<div id = "post-${post.id}">
					<h3 id = "post-title-${post.id}">${post.title}</h3>
					<p id = "post-body-${post.id}">${post.body}</p>
					<button onClick = "editPost(${post.id})" >Edit</button>
					<button onClick = "deletePost(${post.id})">delete</button>
				</div>

			`
		})

		document.querySelector(`#div-post-entries`).innerHTML = entries;

		console.log(document.querySelector(`#div-post-entries`));
	}

	// POST data on our API

	document.querySelector('#form-add-post').addEventListener('submit', (event) => {
		//when the submit event is use, we must add a paramater event to the function to capture the properties of our event. 

		// to change the auto relaod of teh submit method
		event.preventDefault();

		// POST method
			// if we use the post request the fetch methd will return the newly created document

			fetch('https://jsonplaceholder.typicode.com/posts', {
				method: 'POST',

				headers:{
					'content-type' : 'application/json'
				},
				body: JSON.stringify({
					title: document.querySelector('#txt-title').value,
					body: document.querySelector('#txt-body').value,
					userId: 1					
				})
			})
			.then(response => response.json())
			.then(result => {
				console.log(result);

				alert('Post is successfully added!');

				document.querySelector('#txt-title').value = null;

				document.querySelector('#txt-body').value = null;				
			})
	})

		// edit Post functionality

		const editPost = (id) => {
			console.log(id);

			let title = document.querySelector(`#post-title-${id}`).innerHTML;

			let body = document.querySelector(`#post-body-${id}`).innerHTML;

			console.log(title);
			console.log(body);

			document.querySelector('#txt-edit-title').value = title;
			document.querySelector('#txt-edit-body').value = body;
			document.querySelector('#txt-edit-id').value = id;

			// removeAttribute will remove the attribute disable
			document.querySelector('#btn-submit-update').removeAttribute('disabled');
		}

	// updating th post
		document.querySelector('#form-edit-post').addEventListener('submit', (event) => {
				// to prevent the auto reload
				event.preventDefault();

				let id = document.querySelector('#txt-edit-id').value;

				fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {

					method : 'PUT',

					headers: {
						'content-type' : 'application/json'
					},

					body: JSON.stringify({
						title: document.querySelector('#txt-edit-title').value,
						body: document.querySelector('#txt-edit-body').value,
						id: 1
					})
				})
				.then(response =>response.json())
				.then(result => {
					console.log(result);
					alert('The post is successfully updated!');

					document.querySelector(`#post-title-${id}`).innerHTML = document.querySelector('#txt-edit-title').value;

					document.querySelector(`#post-body-${id}`).innerHTML = document.querySelector('#txt-edit-body').value;

					document.querySelector('#txt-edit-title').value = null
					document.querySelector('#txt-edit-body').value = null

					// to disable button
					// setAttribute method adds attriobute to the selected elemetn
					document.querySelector('#btn-submit-update').setAttribute("disabled", true);
				})


			})

		const deletePost = (id) => {
			const selectedElement = document.querySelector(`#post-${id}`);
			selectedElement.remove();
			alert('The post has been deleted successfully!');

		}
