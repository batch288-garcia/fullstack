import {Navigate} from 'react-router-dom';

import { useEffect, useContext} from 'react';

import UserContext from '../UserContext.js';

export default function Logout(){

	// to clear the content of our local storage we hae to use the clear();

	// localStorage.clear();

	const {setUser, unsetUser} = useContext(UserContext);

	useEffect(() => {
		unsetUser();

		setUser({
			id : null,
			isAdmin: null
		});
	})

	return(

		<Navigate to = '/login' />

		)
}