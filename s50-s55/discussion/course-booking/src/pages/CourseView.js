import {useState, useEffect} from 'react';

import {useParams, useNavigate} from 'react-router-dom';

import Swal2 from 'sweetalert2';

import {Container, Row, Col, Button, Card} from 'react-bootstrap';

export default function CourseView(){

	const [name, setName] = useState('');
	const [description, setDescription] =  useState('');
	const [price, setPrice] = useState('');

	const navigate = useNavigate();

	const {courseId} = useParams();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(result => result.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		});
	}, [])

	const enroll = (courseId) => {
		// we are going to fetch the route going to the enroll on our bckend application

		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method : 'POST',
			headers : {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`,
				'Content-Type' : 'Application/json'
			},
			body: JSON.stringify({
				id : courseId
			})
		})
		.then(response => response.json())
		.then(data => {
			// console.log(data)
			if(data){
				Swal2.fire({
					title: 'Successfully enrolled',
					icon : 'success',
					text: 'You have successfully enrolled for this course!'
				})

				navigate('/courses')
			}else{
				Swal2.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again.'
				})	
			}
		})
	}


return(

	<Container>
		<Row>
			<Col>
				<Card>
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        
				        <Card.Subtitle>Descrition:</Card.Subtitle>
				        <Card.Text>{description}</Card.Text>

				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>Php: {price}</Card.Text>

				        <Card.Subtitle>Class Schedule:</Card.Subtitle>
				        <Card.Text>8am - 5pm</Card.Text>

				        <Button onClick = {() => enroll(courseId)} variant="primary">Enroll</Button>
				      </Card.Body>
				    </Card>
			</ Col>
		</ Row>
	</ Container>



	);
}