import {Form, Button, Container, Row, Col} from 'react-bootstrap';

import {useState, useEffect, useContext} from 'react';

import {Link, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext.js';

import Swal2 from 'sweetalert2';

export default function Register() {
	const [firtName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	// we consume the setUser function from the UserContext
	const {setUser} = useContext(UserContext);

	const navigate = useNavigate();

	const [isDisabled, setIsDisabled] = useState(true);

	// we have to use the useEffect in enabling he submit button\
	useEffect(() => {

		if(email !== '' && password !== '' && password.length > 6 && mobileNo.length > 10){
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}

	}, [email, password, mobileNo]);


	// function that will be triggered once we submit the form
	function register(event) {
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method : "POST",
			headers : {
				'Content-Type' : 'Application/json'
			},
			body : JSON.stringify({
							firstName : firtName,
							lastName : lastName,
							email : email,
							password: password,
							mobileNo : mobileNo
						})
		})
		.then(response => response.json())
		.then(data => {
			if(data){
				Swal2.fire({
					title: 'Successfully Registered',
					icon : 'success',
					text: 'Welcome to Zuitt!'
				})

				navigate('/login')
			}else{
				Swal2.fire({
					title: 'Duplicate email found',
					icon: 'error',
					text: 'Please provide different email.'
				})	
			}
		})



		// localStorage.setItem('email', email);
		// setUser(localStorage.getItem('email'));

		// alert("Thank you for registering!");

		// navigate('/');

		// clear input fields
		// setEmail('');
		// setPassword1('');
		// setPassword2('');
	}


return(


	<Container className = "mt-5">
		<Row>
			<Col className = "col-6 mx-auto">
			<h1 className = "text-center">Register</h1>
				<Form onSubmit = {event => register(event)}>

					<Form.Group className="mb-3">
						<Form.Label>First Name</Form.Label>
						<Form.Control 
							type = "text"
							value = {firtName}
							onChange = {event => setFirstName(event.target.value)}
							placeholder = "First Name" 
						/>

					</Form.Group>

					<Form.Group className="mb-3">
						<Form.Label>Last Name</Form.Label>
						<Form.Control 
							type = "text"
							value = {lastName}
							onChange = {event => setLastName(event.target.value)}
							placeholder = "Last Name" 
						/>

					</Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicEmail">
				      <Form.Label>Email address</Form.Label>
				      <Form.Control 
								    type="email"
								    value = {email}
								    onChange = {event => setEmail(event.target.value)}
				      				placeholder="example@mail.com" />
				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicPassword">
				      <Form.Label>Password</Form.Label>
				      <Form.Control 
							      type="password" 
							      value = {password} 
							      onChange = {event => setPassword(event.target.value)}
							      placeholder="Password" />
				    </Form.Group>


				    <Form.Group className="mb-3" controlId="formBasicNumber">
				    	<Form.Label>Mobile no.</Form.Label>
				    	<Form.Control 
				    		type = "number"
				    		value = {mobileNo}
							onChange = {event => setMobileNo(event.target.value)}
				    		placeholder = "+63 ...." 
				    	/>

				    </Form.Group>

				    <p>Have an account already? <Link to = '/login'>Log in here</Link></p>
					
				    <Button variant="primary" 
						    disabled = {isDisabled}
						    type="submit">
				      Register
				    </Button>
			  	</Form>
			</Col>
		</Row>
	</Container>

		)
}