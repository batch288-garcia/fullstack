import {Container, Row, Col} from 'react-bootstrap';

import {Link} from 'react-router-dom';


export default function PageNotFound(){
	return(

		<Container className = " container text-center">
			<Row>
				<Col className = "col-6 mx-auto">
					<h2>Page Not Found</h2>
					<img className = "img-height" src = "https://cdn.svgator.com/images/2022/01/404-page-animation-example.gif" />
					<p>Go back to the <Link to = '/'>homepage</Link></p>
				</Col>
			</Row>
		</Container>

		)
}