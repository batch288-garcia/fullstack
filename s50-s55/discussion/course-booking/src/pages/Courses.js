import coursesData from '../data/courses.js';
import CourseCard from '../components/CourseCard.js'

import {useState, useEffect} from 'react';

export default function Courses() {

	// we console the courses data wether we imported the mock data properly.
	// console.log(coursesData);

	// the prop/courseProp that weill be passed will be in obaject data and the name og the prop will be the namme of the attribute 

	const [courses,setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/activeCourses`)
		.then(result => result.json())
		.then(data => {
			// console.log(data);
			setCourses(data.map(courses => {
				return(

					<CourseCard key = {courses._id} props = {courses} />

					)
			}))
		});
	}, [])


/*
	const courses = coursesData.map(course => {
		return(

				<CourseCard key = {course.id} courseProp = {course}/>
			)
	})*/

	return (
		<>
			<h1 className = 'text-center mt-3' >Courses</h1>
			{courses}
		</>
		)
}