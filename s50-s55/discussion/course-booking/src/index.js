import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

//createRoot() - assigns the element to be managed by React wit its Virtual DOM
const root = ReactDOM.createRoot(document.getElementById('root'));

//render() - displaying the component/react element in to the root
// make sure to mouthn <App />
root.render(
   <React.StrictMode>
     <App />
   </React.StrictMode>

);

// comment later copy sa repo

// const name = 'John Smith';
// const element = <h1>Hello, {name}! </h1>

// root.render(element);