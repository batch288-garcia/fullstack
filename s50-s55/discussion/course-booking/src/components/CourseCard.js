import {Button, Container, Row, Col, Card} from 'react-bootstrap';
import UserContext from '../UserContext.js';

// we hae to import the useState hook from react
import {useState, useEffect, useContext} from 'react';

import {Link} from	"react-router-dom";

export default function CourseCard(props) {
	//object destructuring

	// consume the context of userContext
	const {user} = useContext(UserContext);  

	console.log(props.props);

	const {_id, name, description, price} = props.props;

	// Use t he state hook for thjis component to be able to store its state
	// the states are use to keep track of information related to individual components

			// Syntax : conts [getter, setter] = useState(intiaGetterValue);

	const [count, setCount] = useState(30);
	// console.log(count);
	const [displaySeats, setDisplaySeats] = useState(0);
	const [isDisabled, setIsDisabled] = useState(false);

	// this function will be invoke when the btn enroll is cliocked
	function enroll(){
		if (count === 0) {
		  alert('No more seats!');
		}else {
	      setCount(count - 1);
	      setDisplaySeats(displaySeats + 1);

    	}
	}


	// The function or the side effect in our useEffect hook will invoke or run on the initial loading of our application and when there is/are changes on our dependencies.
	// SYntax:
		// useEffect(side effect, [dependencies])

	useEffect(() => {
		if(count === 0){
			setIsDisabled(true);
		}

	}, [count])

	return(

		<Container className = "mt-3">
			<Row>
				<Col className = "col-12">
					<Card className = "cardHighlight">
					  <Card.Body>
					    <Card.Title>{name}</Card.Title>
					    <Card.Subtitle className="">Descrition:</Card.Subtitle>
					    <Card.Text>
					     {description}
					    </Card.Text>
					    <Card.Subtitle className="">Price:</Card.Subtitle>
					    <Card.Text>Php {price}</Card.Text>
					    <Card.Subtitle className="">Enrollees:</Card.Subtitle>
					    <Card.Text>{displaySeats} Enrollees</Card.Text>

					    	{

					    		user.id !== null ?
					   			 <Button as ={Link} to = {`/courses/${_id}`} variant="primary">Details</Button>
					   			 :
					   			 <Button as = {Link} to = {`/courses/${_id}`} variant="primary">Details</Button>

					    	}

					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>


		);
}