import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import AppNavBar from './components/AppNavBar.js';
import Courses from './pages/Courses.js';
import Home from './pages/Home'
import Register from  "./pages/Register.js";
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/PageNotFound.js';
import CourseView from './pages/CourseView.js';

// The BrowserRouter component will enable us to simulate page nvigating by synchronizing the shown content and the shown URL in the web browser.
// The Routes component holds all our Routes components. It selects w/c 'Route' component to shown based on the URL endpoint.
import {BrowserRouter, Route, Routes, Navigate} from 'react-router-dom';

import {useState, useEffect} from 'react';

// Import userProvider
import {UserProvider} from './UserContext.js';

function App() {

  
  const [user, setUser] = useState({  id: null,
    isAdmin: null});

    useEffect(() => {
      if(localStorage.getItem('token') === null){
        console.log('enter')

        setUser({
               id: null,
               isAdmin: null
             })

       
      }else{
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetail`, {
          method: 'GET',
          headers:{
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then(result => result.json())
        .then(data => {
          console.log(data);
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        });
      }
      
      
    }, [])

    const unsetUser = () => {
      localStorage.clear();
    };

    useEffect(()=> {
      console.log(user);
      console.log(localStorage.getItem('token'));
    }, [user])



    const isAuthenticated = user.id !== null;

     const ProtectedRoute = (element, redirectTo) => {
       return isAuthenticated ? <Navigate to={redirectTo} /> : element;

      };


  return (
  <UserProvider value = {{user, setUser, unsetUser}}>
    <BrowserRouter>
        <AppNavBar />
          <Routes>
            <Route path = '/' element = {<Home/>} />
            <Route path = '/courses' element = {<Courses/>} />
            <Route
                        path="/register"
                        element={ProtectedRoute(<Register />, 'notAccessible')}
                      />
            <Route path="/login" element={ProtectedRoute(<Login />, 'notAccessible')} />
            <Route path = '/logout' element = {<Logout/>} />
            <Route path = '/courses/:courseId' element = {<CourseView/>} />
            <Route path="/notAccessible" element={<PageNotFound />} />
            <Route path="/*" element={<Navigate to="/notAccessible" />} />
          </Routes>
    </BrowserRouter>
  </UserProvider>
  );
}

export default App;
