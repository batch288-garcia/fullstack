import React from 'react';
import {useState} from	'react';

// Create a contect object
// A context Object as the name states is data that can be used to store information that can be shared to other comp[onents within the app.

// The cobntecxt object is a different approch to passsing infromation between components and allows easier access by avoiding the use of prop passing.

// With the help of createContext() method we were abnle to create a context stored in variable UserContext.
const UserContext = React.createContext();

// The 'Provider' component allows other components to consume/use the context and supply necessary information neeeded to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;