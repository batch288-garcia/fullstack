// console.log("hello");

// Section: Document Object Model (DOM)
	// allows us to access or modify the properties of an html element in a webpage.
	// it is standarad on how to get, change, add or delete HTML elements
	// we will be focusing only with DOM in terms of managing forms.
	

	// using the querySelector it can access/get the HTML elemetns
		// CSS selectors top target specfic element
			// id selector(#);
			// class seector(.);
			// tag/type selector(html tags);
			// universal slector(*);
			// attribute selector([attribute]);
// console.log(firstElement);



	// querySelector
	let secondElement = document.querySelector(".full-name");
	console.log(secondElement);

	// querySelector
	let	thirdElement = document.querySelectorAll('.full-name');
	console.log(thirdElement);


	// getEleemnts
	let element = document.getElementById('fullName');
	console.log(element);


	element = document.getElementsByClassName("full-name");
	console.log(element);

// [Section] : Event Listemers
		// Whenever a user interacst with a webpage, this ction is considered as event 
		// working with events is large part of creating interactivuity in a webpage
		// specfic wull be invoked if the event happen

		// function 'addEventListener', it takes two arguments
			// first argument a string identifying the evet 
			// second once the specifiec event occur.

		let fullName = document.querySelector('#fullName');


		let firstElement = document.querySelector('#txt-first-name');

		firstElement.addEventListener('keyup', () => {

		let valueInput = firstElement.value;
			console.log(valueInput);
			 fullName.innerHTML =`${firstElement.value} ${txtLastName.value}`
		});

		let txtLastName = document.querySelector('#txt-last-name');

		txtLastName.addEventListener('keyup', () => {
			console.log(txtLastName.value);

			 fullName.innerHTML =`${firstElement.value} ${txtLastName.value}`
		})

		// Activity starts here 

		let textColor = document.querySelector('#text-color');

		textColor.addEventListener('change', () => {
		  let selectedColor = textColor.value;
		  
		  switch (selectedColor) {
		    case 'red':
		      fullName.style.color = 'red';
		      break;
		    case 'green':
		      fullName.style.color = 'green';
		      break;
		    case 'blue':
		      fullName.style.color = 'blue';
		      break;
		    default:
		      fullName.style.color = 'black';
		      break;
		  }
		});

		/*moree efficient way
			
			let textColor = document.querySelector('#text-color');

			textColor.addEventListener('change', () => {
			  fullName.style.color = textColor.value;
			});

		*/

		// Activity starts here 
